import React from "react";
import { Route, Routes } from "react-router-dom";
import Pokemons from "../pages/Pokemons";
import Login from "../pages/Login";
import AuthLayout from "../layout/AuthLayout";
import AppLayout from "../layout/AppLayout";

function AppRoutes() {
	return (
		<Routes>
			<Route element={<AppLayout />}>
                <Route index element={<Pokemons />} />
			</Route>
			<Route element={<AuthLayout />}>
				<Route path="/login" element={<Login />} />
			</Route>
		</Routes>
	);
}

export default AppRoutes;
