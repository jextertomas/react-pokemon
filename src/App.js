import "./App.css"
import AppRoutes from "./routes/AppRoutes"
import { Link, useNavigate } from "react-router-dom"
import { useEffect, useState } from "react"

function App() {
    const navigate = useNavigate()
    const [credential, setCredential] = useState(localStorage.getItem('credential'))
    useEffect(() => {
        // if (credential != null) {
        //     navigate('/')
        // } else {
        //     navigate('/login')
        // }
    }, [])


	return <AppRoutes />
}

export default App;
