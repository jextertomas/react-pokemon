import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter } from "react-router-dom";
// import { GoogleAPI, GoogleLogin, GoogleLogout } from 'react-google-oauth'
// import { GoogleOAuthProvider } from '@react-oauth/google'

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
	// <React.StrictMode>
    <>
        <BrowserRouter>
            {/* <GoogleOAuthProvider clientId="487704713576-62dt3l1u79gphott9ea85bsp25frfahh.apps.googleusercontent.com"> */}
                <App />
            {/* </GoogleOAuthProvider> */}
        </BrowserRouter>
    </>
	// {/* </React.StrictMode> */}
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


{/* <GoogleAPI
    clientId="487704713576-62dt3l1u79gphott9ea85bsp25frfahh.apps.googleusercontent.com"
    onUpdateSigninStatus={ (e) => { console.log(e) } }
    onInitFailure={(error) => { console.log(error) }}
    fetchBasicProfile="true"
    redirectUri="http://localhost:3000/about"
>

    <div><GoogleLogin /></div>
    <div><GoogleLogout /></div>
    <BrowserRouter>
        <App />
    </BrowserRouter>
</GoogleAPI> */}