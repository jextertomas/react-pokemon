import React from "react"
import { Link, Outlet } from "react-router-dom"
import { useNavigate } from "react-router-dom"

const AppLayout = ({ children }) => {
    const navigate = useNavigate()
	return (
        <div className="bg-gray-900 text-white min-h-screen flex flex-col">
            <header className="bg-gray-800 p-4">
				<nav className="container mx-auto">
					<ul className="flex space-x-10">
						<li>
							<Link to="/" className="text-white bg-slate-700 p-3 rounded-sm px-10 hover:text-gray-300">
								Pokemons
							</Link>
						</li>
                        <li>
                            <a className="text-white bg-slate-700 p-3 rounded-sm px-10 hover:text-gray-300 cursor-pointer" onClick={(e) => {
                                e.preventDefault()
                                localStorage.removeItem('credential')
                                navigate('/login')
                            }}>Logout</a>
							{/* <Link to="/login" className="text-white bg-slate-700 p-3 rounded-sm px-10 hover:text-gray-300">
								Login
							</Link> */}
						</li>
					</ul>
				</nav>
			</header>
			<main className="container mx-auto flex-grow p-4">
                <Outlet />
			</main>
		</div>
	);
};

export default AppLayout;