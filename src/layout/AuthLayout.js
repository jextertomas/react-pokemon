import React from "react";
import { Outlet } from "react-router-dom"

const AuthLayout = ({ children }) => {
	return (
		<div className="bg-gray-900 text-white min-h-screen flex flex-col">
			<main className="container mx-auto flex-grow p-4">
				<Outlet />
			</main>
		</div>
	);
};

export default AuthLayout;
