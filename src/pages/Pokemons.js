import React, { useState, useEffect } from "react";
import axios from "axios";
import Loader from "react-js-loader";
import AutoCompleteComponent from "../components/AutoCompleteComponent";
import PokemonMovesComponent from "../components/PokemonMovesComponent";

function Pokemons() {
	const [pokemonsList, setPokemonsList] = useState([]);
	const [searchInputValue, setSearchInputValue] = useState("");
	const [pokemonData, setPokemonData] = useState(null);
	const [defaultLoadingMessage, setDefaultLoadingMessage] = useState(
		"Pokeball"
	);
	const [htmlContent, setHtmlContent] = useState("");

	function toUpperCaseWords(str) {
		const words = str.split(" ");

		const capitalizedWords = words.map(word => {
			if (word.length > 0) {
				return (
					word.charAt(0).toUpperCase() + word.slice(1).toLowerCase()
				);
			}
			return word;
		});

		return capitalizedWords.join(" ");
	}

	const handleSearchInputChange = value => {
		setSearchInputValue(value);
	};
	// https://aniwave.to/watch/overlord-ii.9n1q/ep-9
	const onClickSearchButton = () => {
		setDefaultLoadingMessage(
			<Loader
				type="bubble-top"
				bgColor={"#000000"}
				color={"#000000"}
				size={30}
			/>
		);
		setPokemonData(null);
		axios
			.get(`https://pokeapi.co/api/v2/pokemon/${searchInputValue}`)
			.then(function(response) {
				setDefaultLoadingMessage("Pokeball");
				setPokemonData(response.data);
			})
			.catch(function(error) {
				setDefaultLoadingMessage("Pokeball");
				setPokemonData(null);
				console.error("Error:", error);
			});
		console.log(pokemonData);
	};

	// Get all pokemon data for pokemo list autocomplete
	const fetchAllPokemons = async () => {
		axios
			.get("https://pokeapi.co/api/v2/pokemon?offset=0&limit=100000")
			.then(function(response) {
				let results = response.data.results.map(
					pokemon => pokemon.name
				);
				setPokemonsList(results);
			})
			.catch(function(error) {
				console.error("Error:", error);
			});
	};

	useEffect(() => {
		axios
			.get("http://node-api.loca.lt/", {
				headers: {
					// "ngrok-skip-browser-warning": "000000",
                    // 'Access-Control-Allow-Origin': "*",
                    'Bypass-Tunnel-Reminder': '000000'
				}
			})
			.then(function(response) {
				console.log(response);
			})
			.catch(function(error) {
				console.error("Error:", error);
			});
		const requestData = {
			"key1": "value1",
			"key2": "value2"
		};
		axios
            .post(`http://26ab-133-125-54-150.ngrok.io/request`, requestData, {
                headers: {
                    // "ngrok-skip-browser-warning": "000000",
                    'Content-Type': 'application/json',
                    // 'Access-Control-Allow-Origin': "*"
                }
            })
			.then(function(response) {
				// Handle the success response
				console.log("POST request successful:", response.data);
			})
			.catch(function(error) {
				// Handle any errors that occurred during the request
				console.error("Error:", error);
			});
		fetchAllPokemons();
	}, []);

	return (
		<div>
			<div className="grid grid-cols-[3fr,1fr] gap-4">
				<div className="w-full">
					<h1 className="text-3xl font-bold mb-4">PokeSearch</h1>
					<AutoCompleteComponent
						data={pokemonsList}
						handleSearchButtonClick={onClickSearchButton}
						onSearchInputChange={handleSearchInputChange}
					/>
					<PokemonMovesComponent
						moves={pokemonData == null ? null : pokemonData.moves}
					/>
				</div>

				<div className="w-full">
					<div className="max-w-full mx-auto bg-white shadow-md overflow-hidden rounded-md">
						{/* Card Header, Pokemon Name */}
						<div className="px-6 py-4 bg-slate-700">
							<h1 className="text-2xl font-semibold text-white">
								{pokemonData == null || !pokemonData.name
									? defaultLoadingMessage
									: toUpperCaseWords(pokemonData.name)}
							</h1>
						</div>

						{/* Card Body */}
						<div className="p-6">
							{/* Pokemon Image */}
							<img
								src={
									pokemonData == null ||
									!pokemonData.sprites ||
									!pokemonData.sprites.other
										? "/pokeball.gif"
										: pokemonData.sprites.other.home
												.front_default
								}
								alt={
									pokemonData == null
										? defaultLoadingMessage
										: pokemonData.name
								}
								className="w-full h-full object-cover"
							/>
							{/* Pokemon Type */}
							<p className="text-gray-600 font-medium text-center">
								Type:{" "}
								{pokemonData == null || !pokemonData.types
									? defaultLoadingMessage
									: pokemonData.types
											.map(type =>
												toUpperCaseWords(type.type.name)
											)
											.join(", ")}
							</p>

							<div className="mt-4 grid grid-cols-[2fr,2fr] gap-1">
								<div className="w-full">
									{/* Pokemon Default Stats */}
									<h3 className="text-md font-semibold text-black">
										Stats:
									</h3>
									<ul className="text-black text-sm">
										{pokemonData == null ||
										!pokemonData.stats
											? defaultLoadingMessage
											: pokemonData.stats.map(stat =>
													<li key={stat.stat.name}>
														{toUpperCaseWords(stat.stat.name)}
														: {stat.base_stat}
													</li>
												)}
									</ul>
								</div>
								{/* <div className="w-full">
									<h5 className="text-md font-semibold text-black">
										Effective Against:
									</h5>
									<h5 className="text-md font-semibold text-black">
										Ineffective Against:
									</h5>
								</div> */}
							</div>
							<div>
								<h1>HTML Content in Iframe</h1>
								<iframe
									title="Embedded Content"
									srcDoc={htmlContent} // Set the HTML content here
									width="100%"
									height="500"
									frameBorder="0"
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

export default Pokemons;
