import React, { useState } from "react";
import { FaFacebook } from "react-icons/fa";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import { isExpired, decodeToken } from "react-jwt"
import { useFacebookLogin } from "react-facebook-login-hook"
import { useNavigate } from "react-router-dom";
import Loader from "react-js-loader"


const Login = () => {
	const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loggingInWithGoogle, setLoggingInWithGoogle] = useState(false)
    const [loggingInWithForm, setLoggingInWithForm] = useState(false)
    const navigate = useNavigate()

	const handleLogin = e => {
		e.preventDefault();
	};

    const { busy, logIn, logOut, getProfile } = useFacebookLogin({ appId: "1293472114865516" });
	const handleFacebookLogin = async () => {
        const response = await logIn();
        console.log(response)
        if (response?.status === "connected") {
            // response.authResponse.accessToken - process access token
            const profile = await getProfile();
            console.log("profile", profile);
        }
        
	};

    const handleGoogleLogInSuccess = (credentialResponse) => {
        setLoggingInWithGoogle(true)
        const payload = decodeToken(credentialResponse.credential)
        localStorage.setItem('credential', payload.email)
        setTimeout(() => {
            navigate('/')
        }, 500)
        // console.log(payload.email)
	    // console.log(decodeToken(credentialResponse.credential))
	}

    const handleGoogleLogInError = () => {
        setLoggingInWithGoogle(false)
        localStorage.removeItem('credential')
	    console.log('Error while logging in')
	}

	return (
		<div className="max-h-screen flex items-center justify-center">
            <div className="bg-white p-8 rounded-sm shadow-md w-96">
				<h2 className="text-2xl font-semibold text-black flex justify-center item-center font-sans">
					PokeAuth
				</h2>
				<form onSubmit={handleLogin}>
					<div className="mb-4">
						<label
							htmlFor="email"
							className="block text-gray-700 font-medium"
						>
							Email
						</label>
						<input
							type="email"
							id="email"
							className="border rounded-sm bg-slate-300 border-slate-300 px-4 py-2 w-full text-black pr-10"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</div>
					<div className="mb-4">
						<label
							htmlFor="password"
							className="block text-gray-700 font-medium"
						>
							Password
						</label>
						<input
							type="password"
							id="password"
							className="border rounded-sm bg-slate-300 border-slate-300 px-4 py-2 w-full text-black pr-10"
							value={password}
							onChange={e => setPassword(e.target.value)}
							required
						/>
					</div>
					<div className="mb-1">
						<button
							type="submit"
							className="bg-red-600 text-white py-2 block px-4 rounded-sm hover:bg-red-700 focus:outline-none focus:bg-red-700 w-full"
						>
							Login
						</button>
					</div>
					<div className="flex items-center">
						<div className="flex-1 h-0.5 bg-gray-300" />
						<div className="mx-4 text-gray-600">Or</div>
						<div className="flex-1 h-0.5 bg-gray-300" />
					</div>
                    <div className="flex flex-col space-y-4">
                        <GoogleOAuthProvider clientId="487704713576-62dt3l1u79gphott9ea85bsp25frfahh.apps.googleusercontent.com">
                            {
                                (loggingInWithGoogle) ? 
                                    <button
                                        disabled
                                        className="bg-white hover:bg-white text-white py-0 px-0 rounded-md flex items-center justify-center border border-gray border-solid"
                                    >
                                        <div style={{ height: '30px' }}>
                                            <Loader type="box-rectangular" bgColor={"#000000"} color={'#000000'} size={30} />
                                        </div>   
                                    </button>: 
                                    <GoogleLogin
                                        text="Log In with Google"
                                        logo_alignment="left"
                                        theme="filled_white"
                                        size="medium"
                                        type="standard"
                                        width={`320`}
                                        auto_select={false}
                                        useOneTap={false}
                                        cancel_on_tap_outside={true}
                                        onSuccess={handleGoogleLogInSuccess}
                                        onError={handleGoogleLogInError}
                                    />
                            }
                            
                        </GoogleOAuthProvider>

                        <button
                            disabled={busy}
							type="button"
							onClick={handleFacebookLogin}
							className="bg-blue-900 hover:bg-blue-800 text-white py-2 px-4 rounded-sm flex items-center justify-center"
						>
							<FaFacebook className="mr-2" /> {busy ? "Please wait..." : "Log in with Facebook"}
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Login;
