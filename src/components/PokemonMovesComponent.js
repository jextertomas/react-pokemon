import React, { useState } from "react";

const PokemonMovesComponent = ({ moves }) => {
	const [moveDetails, setMoveDetails] = useState("");
	const [activeMove, setActiveMove] = useState(null); // State to track the active move

    const toggleMoveDetails = async moveName => {
        setActiveMove(moveName);
		try {
			const response = await fetch(
				`https://pokeapi.co/api/v2/move/${moveName}`
			);
			if (response.ok) {
				const moveData = await response.json();
				setMoveDetails(moveData.effect_entries);
			} else {
				console.error("Failed to fetch move details.");
			}
		} catch (error) {
			console.error("Error fetching move details:", error);
		}
	};

	if (moves === null || typeof moves == "undefined") {
		return null;
	}

	return (
		<div className="grid grid-cols-[2fr,2fr] gap-4 mt-5">
			<div className="w-full h-96 overflow-y-auto move-custom-scrollbar bg-slate-700 rounded-sm">
				{moves.map((moveData, index) =>
					<div key={index}>
						<button
							onClick={() =>
								toggleMoveDetails(moveData.move.name)}
							className={`block w-full text-left px-1 py-3 text-black font-sans text-base font-semibold ${activeMove ===
							moveData.move.name
								? "bg-gray-300 text-black" // Apply different styles for the active move
								: "bg-white hover:bg-gray-300 shadow-lg"}`}
						>
							{moveData.move.name}
						</button>
					</div>
				)}
			</div>
			<div className="w-full h-96 overflow-y-auto move-custom-scrollbar bg-slate-700 rounded-sm">
				<div className="p-5">
					{moveDetails
						? <span>
								{moveDetails.length > 0
									? moveDetails[0].effect
									: "Move Details Not Available..."}
							</span>
						: "Loading move details..."}
				</div>
			</div>
		</div>
	);
};

export default PokemonMovesComponent;
