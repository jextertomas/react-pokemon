import React, { useState, useEffect, useRef } from "react";

function AutoCompleteComponent({
	data,
	handleSearchButtonClick,
	onSearchInputChange
}) {
	const [query, setQuery] = useState("");
	const [results, setResults] = useState([]);
	const [showResults, setShowResults] = useState(false);
	const [selectedIndex, setSelectedIndex] = useState(-1);
	const inputRef = useRef(null);

	useEffect(
		() => {
			const handleKeyDown = e => {
				if (showResults) {
					if (e.key === "ArrowDown") {
						e.preventDefault();
						setSelectedIndex(
							prevIndex =>
								prevIndex < results.length - 1
									? prevIndex + 1
									: prevIndex
						);
					} else if (e.key === "ArrowUp") {
						e.preventDefault();
						setSelectedIndex(
							prevIndex =>
								prevIndex > 0 ? prevIndex - 1 : prevIndex
						);
					} else if (e.key === "Enter") {
						if (
							selectedIndex >= 0 &&
							selectedIndex < results.length
						) {
							handleListItemClick(results[selectedIndex]);
						}
					}

					// Scroll the selected item into view
					if (inputRef.current && selectedIndex > 0) {
						const list = inputRef.current.parentElement.querySelector(
							".custom-scrollbar"
						);
						const selectedItem = list.querySelector(
							`li:nth-child(${selectedIndex + 0})`
                        );
						if (selectedItem) {
							const itemOffsetTop = selectedItem.offsetTop;
							const itemHeight = selectedItem.clientHeight;
                            const listHeight = list.clientHeight;
                            
							if (itemOffsetTop < list.scrollTop) {
								list.scrollTop = itemOffsetTop;
							} else if (
								itemOffsetTop + itemHeight >
								list.scrollTop + listHeight
							) {
								list.scrollTop =
									itemOffsetTop + itemHeight - listHeight;
							}
						}
					}
				}
			};

			window.addEventListener("keydown", handleKeyDown);
			return () => {
				window.removeEventListener("keydown", handleKeyDown);
			};
		},
		[showResults, results, selectedIndex]
	);

	const handleInputChange = event => {
		const inputValue = event.target.value;
		setQuery(inputValue);
		onSearchInputChange(inputValue);

		const filteredResults = data.filter(item =>
			item.toLowerCase().includes(inputValue.toLowerCase())
		);

		setResults(filteredResults);
		setSelectedIndex(-1);
		setShowResults(inputValue !== "");
	};

	const handleListItemClick = value => {
		setSelectedIndex(-1);
		setQuery(value);
		onSearchInputChange(value);
		setShowResults(false);
	};

	const handleInputFocus = () => {
		setShowResults(query !== "");
	};

	const handleInputBlur = () => {
		setSelectedIndex(-1);
		setTimeout(() => {
			setShowResults(false);
		}, 100);
	};

	return (
		<div className="relative">
			<input
				type="text"
				placeholder="Search..."
				className="border rounded-sm bg-slate-300 border-slate-300 px-4 py-2 w-full text-black pr-10"
				value={query}
				onBlur={handleInputBlur}
				onFocus={handleInputFocus}
				onChange={handleInputChange}
				ref={inputRef}
			/>
			<button
				className="absolute top-0 right-0 mt-0 mr-0 px-4 py-2 bg-slate-700 text-white rounded-sm"
				style={{ padding: ".55rem .5rem .55rem .5rem" }}
				onClick={handleSearchButtonClick}
			>
				{/* You can replace the text with an SVG icon or any other search icon */}
				Search
			</button>
			{showResults &&
				results.length > 0 &&
				<div className="absolute z-10 mt-1 w-full bg-white border border-gray-300 rounded-md shadow-lg max-h-60 overflow-y-auto custom-scrollbar">
					<ul>
						{results.map((result, index) =>
							<li
								key={index}
								className={`px-4 py-2 cursor-pointer hover:bg-gray-100 text-black ${index ===
								selectedIndex
									? "bg-gray-100"
									: ""}`}
								onClick={() => handleListItemClick(result)}
							>
								{result}
							</li>
						)}
					</ul>
				</div>}
		</div>
	);
}

export default AutoCompleteComponent;
